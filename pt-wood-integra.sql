/*
 Navicat Premium Data Transfer

 Source Server         : SQL CON
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : pt-wood-integra

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 29/08/2022 11:06:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_barang
-- ----------------------------
DROP TABLE IF EXISTS `m_barang`;
CREATE TABLE `m_barang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jumlah_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_barang
-- ----------------------------
INSERT INTO `m_barang` VALUES (8, 'XYZ-YDSA-DBA-DAD', 'Kayu Berserat', '390', '29-08-2022', '2022-08-29 02:26:22', '2022-08-29 04:01:12');
INSERT INTO `m_barang` VALUES (9, 'XHHUH-IUIF-GFUHIB', 'Triplek 3mm', '49', '29-08-2022', '2022-08-29 03:58:57', '2022-08-29 04:02:47');

-- ----------------------------
-- Table structure for m_mutasi
-- ----------------------------
DROP TABLE IF EXISTS `m_mutasi`;
CREATE TABLE `m_mutasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `indikator` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qty` int(11) NULL DEFAULT NULL,
  `in` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `out` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_mutasi
-- ----------------------------
INSERT INTO `m_mutasi` VALUES (17, '630C39C2E1D27', '2022-08-29', 'masuk', NULL, '29-08-2022', NULL, 'ketrerangan', '2022-08-29 04:01:12', '2022-08-29 04:01:12');
INSERT INTO `m_mutasi` VALUES (18, '630C3A4A16F1D', '2022-08-05', 'keluar', NULL, NULL, '29-08-2022', 'simpab', '2022-08-29 04:02:47', '2022-08-29 04:02:47');

-- ----------------------------
-- Table structure for trx_mutasi_kode_barang
-- ----------------------------
DROP TABLE IF EXISTS `trx_mutasi_kode_barang`;
CREATE TABLE `trx_mutasi_kode_barang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kode_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 130 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of trx_mutasi_kode_barang
-- ----------------------------
INSERT INTO `trx_mutasi_kode_barang` VALUES (108, '630C17B79F85C', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:36:09', '2022-08-29 01:36:09');
INSERT INTO `trx_mutasi_kode_barang` VALUES (109, '630C18768805A', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:38:02', '2022-08-29 01:38:02');
INSERT INTO `trx_mutasi_kode_barang` VALUES (110, '630C189FE2588', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:38:44', '2022-08-29 01:38:44');
INSERT INTO `trx_mutasi_kode_barang` VALUES (111, '630C18B701D1C', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:39:07', '2022-08-29 01:39:07');
INSERT INTO `trx_mutasi_kode_barang` VALUES (112, '630C18D913291', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:39:40', '2022-08-29 01:39:40');
INSERT INTO `trx_mutasi_kode_barang` VALUES (113, '630C18D913291', 'XFB-DANDBAND-DAD1', 'BARANG TESTING Baru', '2022-08-29 01:39:44', '2022-08-29 01:39:44');
INSERT INTO `trx_mutasi_kode_barang` VALUES (118, '630C1A4DBCF89', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:46:18', '2022-08-29 01:46:18');
INSERT INTO `trx_mutasi_kode_barang` VALUES (119, '630C1A4DBCF89', 'XFB-DANDBAND-DAD1', 'BARANG TESTING Baru', '2022-08-29 01:46:27', '2022-08-29 01:46:27');
INSERT INTO `trx_mutasi_kode_barang` VALUES (120, '630C1ACA20F55', 'XFB-DANDBAND-DAND', 'BARANG TESTING 123', '2022-08-29 01:48:00', '2022-08-29 01:48:00');
INSERT INTO `trx_mutasi_kode_barang` VALUES (121, '630C1ACA20F55', 'XFB-DANDBAND-DAD1', 'BARANG TESTING Baru', '2022-08-29 01:48:04', '2022-08-29 01:48:04');
INSERT INTO `trx_mutasi_kode_barang` VALUES (127, '630C2CEFC9D86', 'XYZ-YDSA-DBA-DAD', 'Kayu Berserat', '2022-08-29 03:05:25', '2022-08-29 03:05:25');
INSERT INTO `trx_mutasi_kode_barang` VALUES (128, '630C39C2E1D27', 'XYZ-YDSA-DBA-DAD', 'Kayu Berserat', '2022-08-29 04:00:36', '2022-08-29 04:00:36');
INSERT INTO `trx_mutasi_kode_barang` VALUES (129, '630C3A4A16F1D', 'XHHUH-IUIF-GFUHIB', 'Triplek 3mm', '2022-08-29 04:02:33', '2022-08-29 04:02:33');

SET FOREIGN_KEY_CHECKS = 1;
