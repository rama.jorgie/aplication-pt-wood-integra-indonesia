<?php

namespace App\Http\Controllers;

use App\Models\BarangModel;
use App\Models\Mutasi_Model;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
   public function beranda(){
    $data['barang'] = BarangModel::count();
    $data['mutasi'] = Mutasi_Model::count();
    return view('layoute-page.beranda.beranda',$data);
   }
}
