<?php

namespace App\Http\Controllers;

use App\Models\BarangModel;
use App\Models\MBarang_Model;
use App\Models\Mutasi_Model;
use App\Models\Trx_Mutasi_Kode_Barang_Model;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class MutasiController extends Controller
{
    public function create_mutasi(){
        $data['kode_barang'] = BarangModel::all();
        $data['key'] = strtoupper(uniqid());
        return view('layoute-page.mutasi.create-mutasi',$data);
    }

    public function save_kode_barang($id ,$kode){
        // return $id;
        $nama_barang = BarangModel::where('kode_barang',$kode)->first();
        $add = new Trx_Mutasi_Kode_Barang_Model();
        $add->no_bukti = $id;
        $add->kode_barang = $kode;
        $add->nama_barang = $nama_barang->nama_barang;
        $add->save();

        $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->get();
       
        foreach ($data as $value) {
            $html = '
            <table class="table" style= >
                <tr  style="text-align:center ;">
                    <th>ID</th>
                    <th>Kode</th>
                    <th>Nama Barang</th>
                    <th>Jumlah Barang</th>
                </tr>
                <tr  style="text-align:center ;">
                    <td>'.$value->id.'</td>
                    <td>'.$value->kode_barang. '</td>
                    <td>' . $value->nama_barang . '</td>
                    <td><input class="form-control" type="text" name="jumlah_barang[]" required></td>
                </tr>
            </table>';
        }
        return $html;
    }
    public function delete_kode_barang($id){
        // return $id;  
        $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->first();
        $data->delete();
        // $data = Trx_Mutasi_Kode_Barang_Model::where('no_bukti', $id)->get();
        $html ='';
        return $html;
    }
    public function save_mutasi(Request $request){
        // return $request->all();
        // $data_stok = BarangModel::where
        $data_barang = Trx_Mutasi_Kode_Barang_Model::where('no_bukti', $request->no_bukti)->get();
        // return $data;
        for ($i=0; $i <count($request->jumlah_barang) ; $i++) { 
        // foreach ($data_barang as $value){
            $stok_barang = BarangModel::where('kode_barang',$data_barang[$i]->kode_barang)->first();
            if ($request->indikator == 'masuk'){
                $sisa = $stok_barang->jumlah_barang + $request->jumlah_barang[$i];
            }else{
                $sisa = $stok_barang->jumlah_barang - $request->jumlah_barang[$i];
            }      
            $nilai = BarangModel::where('kode_barang', $data_barang[$i]->kode_barang)->update([
                'jumlah_barang' => $sisa,
            ]);
        // }
        }
        // return $nilai;

        $add = new Mutasi_Model();
        $add->no_bukti = $request->no_bukti;
        $add->tanggal = $request->tanggal;
        $add->indikator = $request->indikator;
        if ($request->indikator == 'masuk'){
            $add->in = date('d-m-Y');
        }else{
            $add->out = date('d-m-Y');
        }
        $add->keterangan = $request->keterangan;
        $add->save();
        return redirect()->back()->with('save','save');

    }
    public function report_mutasi(Request $request){
        if ($request->tgl_awal != Null || $request->tgl_akhir != Null){ 
            $data_mutasi = Mutasi_Model::whereBetween('tanggal', [$request->tgl_awal, $request->tgl_akhir])->get();
            foreach ($data_mutasi as $value) {
                $nama_barang = Trx_Mutasi_Kode_Barang_Model::select('nama_barang')->where('no_bukti', $value->no_bukti)->get();
                $barang = [];
                for ($i = 0; $i < count($nama_barang); $i++) {
                    $barang[] = $nama_barang[$i]->nama_barang;
                }

                $data_barang = implode(' || ', $barang);
                $mutasi[] = [
                    'id' => $value->id,
                    'no_bukti' => $value->no_bukti,
                    'nama_barang' => $data_barang,
                    'tanggal' => $value->tanggal,
                    'indikator' => $value->indikator,
                    'in' => $value->in,
                    'out' => $value->out,
                    'keterangan' => $value->keterangan
                ];
            }
        }else{
            // $mutasi = Mutasi_Model::join('trx_mutasi_kode_barang', 'trx_mutasi_kode_barang.no_bukti','=','m_mutasi.no_bukti')
            // ->select('m_mutasi.no_bukti','m_mutasi.tanggal', )
            // ->groupBy('m_mutasi.no_bukti', 'm_mutasi.tanggal')->get();
            // $mutasi = [];
            $data_mutasi = Mutasi_Model::all();
            foreach ($data_mutasi as $value){
                $nama_barang = Trx_Mutasi_Kode_Barang_Model::select('nama_barang')->where('no_bukti',$value->no_bukti)->get();
                $barang = [];
                for ($i=0; $i <count($nama_barang) ; $i++) { 
                    $barang[] =$nama_barang[$i]->nama_barang;
                }
                $data_barang = implode(' || ', $barang);
                $mutasi[] =[
                    'id' => $value->id,
                    'no_bukti' => $value->no_bukti,
                    'nama_barang' => $data_barang,
                    'tanggal' => $value->tanggal,
                    'indikator' => $value->indikator,
                    'in' => $value->in,
                    'out' => $value->out,
                    'keterangan' => $value->keterangan
                ];
            }

        }
        // dd($mutasi);
        // return $mutasi;
        if ($mutasi != []){
            $mutasi = $mutasi;
        }else{
            $mutasi =[];
        }
        $data['trx_kode_barang'] = Trx_Mutasi_Kode_Barang_Model::all();
        $data['mutasi'] = Mutasi_Model::all();
        $data['barang'] = BarangModel::all();
        if ($request->ajax()) {
            return DataTables::of($mutasi)->addIndexColumn()->addColumn('action', function ($row) {
                $data =
                    '
                        <a href ="/delete-mutasi/' . $row['no_bukti'] . '" id="btn-hapus" > <span id="hapus" class="badge bg-danger btn-sm" "><i class="fa fa-trash"></i>Delete</span> </a> 
                        <a href = "/update-mutasi/' . $row['no_bukti'] . '" > <span id="hapus" class="badge bg-warning btn-sm" "><i class="fa fa-trash"></i>Update</span> </a> 
                    ';
                
                return $data;
            })->rawColumns(['action'])
            ->make(true);
        }
        return view('layoute-page.mutasi.browse-mutasi',$data);  
    }
    public function get_barang_mutasi($id){
        return 'INI BARANG';
    }

    public function delete_mutasi($id){
        $m_mutasi = Mutasi_Model::where('no_bukti',$id)->first();
        $m_mutasi->delete();
        $trx_mutasi = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->delete();
        return redirect()->back();
    }

    public function update_mutasi($id){
        $data['kode_barang'] = BarangModel::all();

        $data['mutasi'] = Mutasi_Model::where('no_bukti',$id)->first();
        $data['trx_mutasi'] = Trx_Mutasi_Kode_Barang_Model::where('no_bukti',$id)->get();
        return view ('layoute-page.mutasi.updated-mutasi',$data);
    }
    public function save_update_mutasi(Request $request){

        Mutasi_Model::where('no_bukti',$request->no_bukti)->update([
            'tanggal' => $request->tanggal,
            'indikator' => $request->indikator,
            'qty' => $request->qty,
            'keterangan' => $request->keterangan,
        ]);

        return redirect()->route('report-mutasi');
    }
    public function delete_item_mutasi($id){
        // return $id;
        $trx_mutasi = Trx_Mutasi_Kode_Barang_Model::where('id',$id)->first();
        $trx_mutasi->delete();
        return redirect()->back();
    }
     
}
