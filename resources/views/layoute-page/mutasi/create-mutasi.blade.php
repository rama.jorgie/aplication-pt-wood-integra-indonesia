@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('dashboard')}}/css/pages/toastify.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
<script>
    $(function() {
        $("#datepicker").datepicker();
    });
</script>

<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>
    @if ($message = Session::get('save'))
    <script>
        alert_add();
    </script>
    @endif

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Form Tambah Mutasi (Transactional)</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah data yang diperoleh sistem! </p>
                </div>
            </div>
        </div>

        <section class="section" id="">
            <div class="card">
                <table style="margin-top: 20px;">
                    <tr style="text-align:center ;">
                        <td><img src="http://woodoneintegra.com/wp-content/uploads/2020/02/logo-2.png" width="100px" alt="" srcset=""></td>
                        <td>
                            <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Form Tambah Data Mutasi</h4>
                            <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Di PT Wood Integra Indonesia</h4>
                        </td>
                        <td><img src="http://woodoneintegra.com/wp-content/uploads/2020/02/logo-2.png" width="100px" alt="" srcset=""></td>

                    </tr>
                </table>
                <hr>
                <div class="card-body">
                    <div style="text-align:right ;">
                        <a href="{{route('report-mutasi')}}"><button class="btn btn-success">Lihat Mutasi Tersimpan</button></a>
                    </div>
                    
                    <form id="form_post" action="{{route('save-mutasi')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" form-group">
                                    <label for="basicInput">No.Bukti</label>
                                    <input type="text" class="form-control" id="no_bukti" name="no_bukti" value="{{$key}}" readonly>
                                </div>
                                <div class=" form-group">
                                    <label for="helpInputTop">Tanggal</label>
                                    <input type="date" class="form-control" id="datepicke" name="tanggal" required>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group ">
                                            <label for="exampleFormControlSelect1">Indikator</label>
                                            <select class="form-control" id="exampleFormControlSelect1" name="indikator">
                                                <option value="masuk">Barang Masuk</option>
                                                <option value="keluar">Barang Keluar</option>
                                            </select>
                                        </div>
                                    </div>
                                   

                                    <div class="col-md-9">
                                        <div class=" form-group">
                                            <label for="exampleFormControlSelect1">Kode Barang</label>
                                            <select class="form-control" id="kode_barang" name="kode_barang">
                                                <option selected disabled>Pilih Kode Barang</option>
                                                @foreach ($kode_barang as $kode)
                                                <option value="{{$kode->kode_barang}}">{{$kode->nama_barang}} - Kode :{{$kode->kode_barang}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group ">
                                            <label for="disabledInput">&nbsp;</label><br>
                                            <span id="tambah-barang" class="btn btn-primary">Tambahkan</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div style="margin-bottom: 7px;">
                                <label for="exampleFormControlTextarea1">Barang Dipilih : <a id="clear" href="#"><span class="badge bg-danger">Clear</span></a> </label>
                            </div>
                            <div style="margin-bottom:10px ;" id="hasil">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="exampleFormControlTextarea1">Keterangan</label>
                                    <textarea class="form-control" name="keterangan" id="exampleFormControlTextarea1" rows="3"></textarea>
                                </div>
                            </div>
                            <br>

                            <div id="btn-save" style="text-align:center ;margin-top:10px ;">
                                <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                                <button id="simpan" class="btn btn-primary">Simpan</button>
                                <!-- <button id="btn-back" class="btn btn-danger">Kembali</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

    @include('main-page.footer')
</div>
@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="{{URL::asset('dashboard')}}/js/extensions/toastify.js"></script>
<script>
    function alert_add() {
        Swal.fire(
            'Saved!',
            'Barang Berhasil Disimpan.',
            'success'
        )
    }
</script>
<script>
    flatpickr('#tanggal', {
        dateFormat: 'd-m-Y'
    });
</script>
<script>
    $(document).ready(function() {

        $('#tambah-barang').click(function() {
            var kode_barang = $('#kode_barang').val();
            var id = $('#no_bukti').val();

            console.log(kode_barang)
            $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "/save-kode-barang/" + id + '/' + kode_barang,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(data) {
                    console.log(data)
                    $('#hasil').show();

                    $('#hasil').append(data)

                }

            });
        });
        $('#clear').click(function() {
            var id = $('#no_bukti').val();
            console.log('celar')
            $.ajax({
                type: "GET",
                enctype: 'multipart/form-data',
                url: "/delete-kode-barang/" + id,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 800000,
                success: function(data) {
                    console.log(data)
                    $('#hasil').empty();

                }

            });
        });



    })
</script>