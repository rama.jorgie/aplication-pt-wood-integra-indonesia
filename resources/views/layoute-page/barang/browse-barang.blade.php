@extends('main-page.main')
@section('content')
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.3.0/js/dataTables.responsive.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.3.0/css/responsive.dataTables.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<link rel="stylesheet" href="{{URL::asset('backend')}}/css/pages/toastify.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>


<div id="main">
    <header class="mb-3">
        <a href="#" class="burger-btn d-block d-xl-none">
            <i class="bi bi-justify fs-3"></i>
        </a>
    </header>

    <div class="page-heading">
        <div class="page-title">
            <div class="row">
                <div class="col-12 col-md-6 order-md-1 order-last">
                    <h3 style="color:white ;">Data Directory Barang PT Wood Interga</h3>
                    <p class="text-subtitle" style="color: white;">Berikut adalah beberapa Data yang diperoleh sistem!!</p>
                </div>
            </div>
        </div>
        @if ($message = Session::get('hapus'))
        <script>
            alert_hapus();
        </script>
        @endif
        @if ($message = Session::get('ditambah'))
        <script>
            alert_ditambah();
        </script>
        @endif
        <section class="section" id="table_user">
            <div class="row" id="table-head">
                <div class="col-12">
                    <div class="card">
                        <table style="margin-top: 20px;">
                            <tr style="text-align:center ;">
                                <td><img src="http://woodoneintegra.com/wp-content/uploads/2020/02/logo-2.png" width="100px" alt="" srcset=""></td>
                                <td>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">List Data Barang</h4>
                                    <h4 style="text-align:center ;color: #435EBE;font-weight: bold;">Di PT Wood Integra Indonesia</h4>
                                </td>
                                <td><img src="http://woodoneintegra.com/wp-content/uploads/2020/02/logo-2.png" width="100px" alt="" srcset=""></td>
                            </tr>
                        </table>
                        <hr>
                        <div class="card-content p-3">
                            <div style="text-align: right;margin-bottom: 5px;margin-right: 5px;">
                                <a href="{{route('create-barang')}}"><button class="btn btn-primary">Tambah Barang</button></a>
                            </div>
                            <table id="data_barang" class="display  nowrap" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kode Barang</th>
                                        <th>Nama Barang</th>
                                        <th>Kategori</th>
                                        <th>Jumlah</th>
                                        <th>Tanggal</th>
                                        <th>Update</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

</div>



@endsection
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script>
    function alert_hapus() {
        Swal.fire(
            'Deleted!',
            'Barang Berhasil Dihapus.',
            'success'
        )
    }

    function alert_ditambah() {
        Swal.fire(
            'Saved!',
            'Barang Berhasil Diupdated.',
            'success'
        )
    }
</script>
<script>
    $(document).ready(function() {
        $('#btn-tambah-user').click(function() {
            $('#table_user').hide();
            $('#form_tambah').show();
        });
        $('#btn-back').click(function() {
            $('#table_user').show();
            $('#form_tambah').hide();
        });
        $('#btn-hapus').click(function() {
            console.log('hapus');
        });

    })
</script>
<script>
    $(function() {

        var sort = $('#kecamatan');


        function loadDataTable() {

            $('#data_barang').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                scrollX: true,
                order: [
                    [5, 'DESC']
                ],

                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        render: function(data) {
                            return '<span class="badge bg-primary">' + data + '</span>';
                        }
                    },

                    {
                        data: 'kode_barang',
                        name: 'kode_barang',
                        render: function(data) {
                            return '<span class="badge bg-success">' + data + '</span>';
                        }
                    },
                    {
                        data: 'nama_barang',
                        name: 'nama_barang',
                        render: function(data) {
                            return data.toUpperCase();;
                        }
                    },
                    {
                        data: 'jumlah_barang',
                        name: 'jumlah_barang',
                        render: function(data) {
                            return 'Barang';
                        }
                    },
                    {
                        data: 'jumlah_barang',
                        name: 'jumlah_barang',
                        render: function(data) {
                            return data.toUpperCase() + ' Barang';
                        }

                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal',
                    },
                    {
                        data: 'updated_at',
                        name: 'updated_at',
                        render: function(data) {
                            var data = new Date();
                            var date_update = data.toDateString();
                            return date_update
                        }
                    },
                    {
                        data: 'action',
                        name: 'action',
                    }


                ]
            });

        }

        loadDataTable();

        kecamatan.change(function() {
            // alert('ok')
            loadDataTable();
        });



    });
</script>