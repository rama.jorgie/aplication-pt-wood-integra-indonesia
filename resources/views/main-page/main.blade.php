<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from zuramai.github.io/mazer/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 10 May 2022 01:52:27 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Mazer Admin Dashboard</title>

    <link rel="stylesheet" href="{{URL::asset('dashboard')}}/css/main/app.css">
    <link rel="stylesheet" href="{{URL::asset('dashboard')}}/css/main/app-dark.css">
    <link rel="shortcut icon" href="{{URL::asset('dashboard')}}/images/logo/favicon.svg" type="image/x-icon">
    <link rel="shortcut icon" href="{{URL::asset('dashboard')}}/images/logo/favicon.png" type="image/png">

    <link rel="stylesheet" href="{{URL::asset('dashboard')}}/css/shared/iconly.css">
    <style>
        /* Navigasi Custom CSS */

        .toggle-menu .leftbar:hover .c-avatar img {
            margin-left: 25%;
            width: 100px !important;
        }

        .toggle-menu .leftbar:hover .c-name {
            display: inline-block !important;
            margin-top: 15px;
            padding: 10px;
            width: 100%;
            text-align: center;
        }

        .toggle-menu .leftbar:hover .c-menu-header {
            display: inline-block !important;
        }

        .toggle-menu .leftbar .c-name,
        .toggle-menu .leftbar .c-menu-header {
            display: none !important;
        }

        .toggle-menu .leftbar .c-avatar {
            margin-left: 15px;
            margin-top: 50px;
            display: block;
            width: 100%;
        }

        .toggle-menu .leftbar .c-avatar img {
            width: 50px !important;
        }

        .c-profil {
            padding: 10px 0 10px 0;
        }

        .c-profil .c-avatar {
            margin-left: 25px;
            display: inline-block;
            width: 30%;
        }

        .c-profil .c-avatar img {
            border-radius: 50%;
            margin-top: -50px;
        }

        .c-profil .c-name {
            display: inline-block;
            width: 50%;
            line-height: 1;
        }

        .c-profil .c-name span {
            font-size: 11px;
        }

        .c-menu-header {
            padding-left: 30px;
            font-weight: bold;
        }
    </style>

</head>

<body>
    <div id="app">
        @include('main-page.sidebar')
    </div>
         @yield('content')
    </div>
    <script src="{{URL::asset('dashboard')}}/js/app.js"></script>

    <script src="{{URL::asset('dashboard')}}/js/pages/dashboard.js"></script>

</body>


<!-- Mirrored from zuramai.github.io/mazer/demo/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 10 May 2022 01:52:53 GMT -->

</html>